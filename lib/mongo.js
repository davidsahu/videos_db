const { MongoClient, ObjectId } = require('mongodb');
const { config } = require('../config');

//Garantiza en caso de existir caracteres especiales
const USER = encodeURIComponent(config.dbUser);
const PASSWORD = encodeURIComponent(config.dbPassword);