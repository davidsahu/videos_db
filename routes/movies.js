const express = require('express');
const MoviesService = require('../service/movies');

function moviesApi(app) {
  const router = express.Router();
  app.use('/api/movies', router);

  const movieService = new MoviesService();

  router.get('/', async function(req, res, next) {
    const { tags } = req.query;
    try {
      const movies = await movieService.getMovies({ tags });
      res.status(200).json({
        data: movies,
        message: 'movies listed'
      });
    } catch (error) {
      next(error);
    }
  });

  router.get('/:movieId', async function(req, res, next) {
    const { movieId } = req.params;
    try {
      const movies = await movieService.getMovie(movieId);
      res.status(200).json({
        data: movies,
        message: 'movies retrived'
      });
    } catch (error) {
      next(error);
    }
  });

  router.post('/', async function(req, res, next) {
    const { body: movie } = req;

    try {
      const createMovieId = await movieService.createMovieId({ movie });
      res.status(201).json({
        data: createMovieId,
        message: 'movie created'
      });
    } catch (error) {
      next(error);
    }
  });

  router.put('/:movieId', async function(req, res, next) {
    const { movieId } = req.params;
    const { body: movie } = req;

    try {
      const updatedMovieId = await movieService.updatedMovieId({
        movieId,
        movie
      });
      res.status(200).json({
        data: updatedMovieId,
        message: 'movies updated'
      });
    } catch (error) {
      next(error);
    }
  });

  router.delete('/:movieId', async function(req, res, next) {
    const { movieId } = req.params;
    try {
      const deletedMovieId = await movieService.deletedMovieId({ movieId });
      res.status(200).json({
        data: deletedMovieId,
        message: 'movies deleted'
      });
    } catch (error) {
      next(error);
    }
  });

  router.patch('/:movieId', async function(req, res, next) {
    const { movieId } = req.params;
    const { body: movie } = req;
    try {
      const partialUpdateMovideId = await movieService.partialUpdateMovie({
        movieId,
        movie
      });
      res.status(200).json({
          data: partialUpdateMovideId,
          message: 'mavie partial update'
      })
    } catch (error) {
      next(error);
    }
  });
}

module.exports = moviesApi;
