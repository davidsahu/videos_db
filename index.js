const express = require('express');
const app = express();

const { config } = require('./config/index');
const moviesApi = require('./routes/movies.js');

moviesApi(app);

// app.get('/', function(req, res) {
//   res.send('hello wordl');
// });

// app.get('/json', function(req, res) {
//   res.send({ hello: 'world' });
// });

// app.get('/:year', (req, res) => {
//     let year = req.params.year;
//     if(year % 4 === 0 && year % 100 !== 0 || year % 400 === 0) {
//         res.send("el"+req.params.year+"es biciesto")
//     } else {
//         res.send("el"+req.params.year + "no es biciesto")
//     }
// })

app.listen(config.port, function() {
    console.log(`Listening http://localhost:${config.port}`);
});